+++

title = "Home"
description = "Home page"
sort_by = "weight"
in_search_index = true
render = true
template = "section.html"

#[extra]
#
#headerImg = "homeMobile.jpg"
#headerImgAlt = "Image of submerged iceburg"
#headerHeadline = "That's smart."
#headerMain = "SMART grows revenue -- not headcount. It finds revenue in places you'd never think to look, and it uses actionable intelligence and innovation to drive growth through exceptional employee and customer experiences. If you're looking for a new way to stand out and grow your business, you'll probably like SMART. Want more? Contact Us."
#
+++

<div class="featuresBlockOne">

## ACTIONABLE INTELLIGENCE

Receive ongoing objective actionable market intelligence. 

## INNOVATIVE THINKING

Uncover hidden revenue and increase loyalty by having something new to say.

## SMART GROWTH
Grow revenue without increasing headcount. 

## EXCEPTIONAL EXPERIENCES
Deliver high-value solutions and better experiences to everyone.
</div>

<div class="featuresBlockTwo">
Grow revenue--not headcount--with a scalable differentiator that delivers proven ROI for minimal risk. Contact Us. Get SMART.
