+++

title = "Smart Tools"
description = ""
weight = 0
render = true
template = "page.html"
[extra]
headerImg = "toolsMobile.jpg"
headerImgAlt = "Image of an F1 racecar steering wheel."
headerHeadline = "smart tools."
headerMain = "Proven, seamless and scalable. SMART tools deliver quick and painless ROI with zero Assigned commitments. We've added everything you need to get growing, but left out the risk and disruption. That's SMART. Want more? Contact Us."

+++

<div class="features">

## SMART Content
Example: SMART Temp finds out still using excel. VAR downloads our SMART engagement templates and timelines to start a conversation and keep it going.  

To keep your audience interested and your customers loyal, you need to remain relevant and timely. But why reinvent the wheel? Download our free templates and timelines to say the right thing at the right time.
</div>
<div class="features">

## SMART Assessments
At the foundation of this platform is our SMART Methodology. SMART Methodology is a series of online, proprietary ERP assessments the staffing company uses to outpace the competition and service their customers better. 

These assessments have transformed the A&F staffing value chain. And the relationship between staffing and resellers has naturally created opportunities for additional install-base touch-points, new services offerings, revenue growth, and perhaps most importantly, a story all your own. 

(test question example)

Our list of publishers is growing and evolving. Here is a partial list of software publishers covered by our SMART Methodology.  

(publisher logos)
</div>
<div class="features">
