+++

title = "About"
description = "About Us web page"
weight = 0
render = true
template = "page.html"

[extra]

headerImg = "connectionsMobile.jpg"
headerImgAlt = "alt img"
headerHeadline = "Smart vision."
headerMain = "Growth without hiring<br>Relationships matter<br>Methodology is essential"

+++

<div class="mask"></div>
<div class="featuresBlockOne">
<div class="feature">

## Growth Without Hiring
SMART generates revenue from places you'd never think to look, allowing you to increase revenue without increased headcount.

</div>

<div class="feature">

## Relationships Matter
We have connected previously unseen and unconnected industries that have people asking: "What took you so long?"

</div>

<div class="feature">

## Methodology is Essential
In a world where value matters and companies seek to create their own narrative, a practical, measurable and verifiable methodology has never been more important.
</div>
</div>

<div class="referralsBlock">

## What others are saying

<div class="quote">

<div class="quoteImg">

Brittenford img

</div>

<div class="quoteSource">

Brian Dietz, President, Brittenford Systems

</div>

<div class="quoteMain">

"This new added service offering has definitely added positive professional service value to our clients and at the same time with minimal cost added significant new revenues to our company.  Today, we are proud to say that our  existing base of  clients have been impacted in a positive manner with this new added service and a significant side benefit has been that we are finding new sales and services opportunities every day within our base and with new clients. Partnering with Nsight was one of the best decisions we have made as an executive team.”

</div>

</div>

<div class="quote">

<div class="quoteImg">

Altico img

</div>

<div class="quoteSource">

Rich Maloney, CEO, Altico Advisors

</div>

<div class="quoteMain">

“Altico Advisors, since day one, has committed to deliver innovative, value-added services that are focused on accelerating our client’s growth and success and at the same time delivering operational efficiencies. The SMART platform does just that.  It enables a channel partner, like Altico Advisors, to add a targeted staffing service offering to its business model as a simple, low-risk extension of our set of services uniquely tailored for our existing client base."

</div>
</div>

<div class="quote">

<div class="quoteImg">

Lanac img

</div>

<div class="quoteSource">

Dale May, CEO, InterDyn LANAC Technology

</div>

<div class="quoteMain">

"As LANAC continues to migrate our business model to be more services driven, our partnership with Nsight adds a natural, logical service extension that delivers a unique value-add which immediately makes a positive impact to our existing client’s daily operation."

</div>
</div>
</div>

<div class="featuresBlockTwo">
<div class="feature">

## How we do it
By combining the $130 Billion staffing industry and the ERP Reseller industries, we create partnerships that gather intelligence and exchange actionable and specific information for business growth to create a new narrative and lower customer acquisition and retention costs. 
</div>

<div class="feature">

## Our SMART Methodology
At the foundation of this platform is our SMART Methodology. SMART Methodology is a series of online, proprietary ERP assessments the staffing company uses to outpace the competition and service their customers better. These assessments have transformed the A&F staffing value chain. And the relationship between staffing and resellers has naturally created opportunities for additional install-base touch-points, new services offerings, revenue growth, and perhaps most importantly, a story all your own. 

(test question example)

Image of test question

Our list of publishers is growing and evolving. Here is a partial list of software publishers covered by our SMART Methodology.  
quickbooks image logo.jpg
</div>
</div>
