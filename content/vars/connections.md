+++
title = "Connections"
description = ""
weight = 0
render = true
template = "page.html"
[extra]
headerImg = "connections.jpg"
headerImgAlt = "Image of a person standing on top of a mountain."
headerHeadline = "now you get it."
headerMain = "SMART gets you connected to your market in ways you never imagined.  Partnership with us opens up a flow of risk-free market intelligence VARs have needed for years. Get it? That’s being connected. That’s being SMART."

+++

<div class="features">

## SMART 0pportunities
Whether it's finding new leads or retaining existing clients, intelligence and communication are key. For years we have worked with Top 100 ERP Resellers to show them new and interesting ways to lower customer acquisition costs, create a new customer experience and grow their installed base.
</div>

<div class="features">

## Partner Benefits
* Steady flow of new business leads
* Keep in touch with your install base on a whole new level helping you hold onto the business you've worked so hard to gain.
* Understand what's going on with companies in your market.
* Learn who is ready to move to the cloud.
* Gain insight into what companies take excessive time to close the books, who's still budgeting with Excel, and who has a new CFO
* Find consulting business with companies inside and outside your existing installed base.
</div>
