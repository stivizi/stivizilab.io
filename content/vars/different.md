+++

title = "Be Different"
description = ""
weight = 0
render = true
template = "page.html"

[extra]

headerImg = "different.jpg"
headerImgAlt = "Image of tulips in a field."
headerHeadline = "that's different."
headerMain = "We get it. Everyone wants more value and better experiences. So where‘s the innovation? Given the size of the industry, there’s surprisingly little to be found. SMART can change that.  SMART gives you something fresh to say and delivers a set of tools that sets you apart from the crowd. It’s a breath of fresh air. It’s SMART.  Want more? Contact us."

+++

<div class="features">

## How are we different?
SMART is the only ERP systems and cross-industry business acceleration platform in existence. For you this means a different conversation. One that forges deeper relationships and sets you apart from your competition.  For your customers, this means SMART Certified talent they trust. :Learn more by contacting us.
</div>
